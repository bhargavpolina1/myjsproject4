function mapObjects(obj = {},cb = undefined) {
    if (Object.keys(obj).length === 0 || cb === undefined){
        return {}
    }

    else{
        newObj = {}
        for (let keys in obj){
        result = cb(obj[keys])
        newObj[keys] = result
     }

     return newObj
    }

    }
    


module.exports = mapObjects;