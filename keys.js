function keys(obj = {}) {
    if (Object.keys(obj).length === 0) {
        return []
    }
    else {
        let keysArray = []
        for (let keys in obj) {
            keysArray.push(keys)
        }

        return keysArray
    }

}


module.exports = keys;