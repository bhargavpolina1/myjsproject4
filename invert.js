function invert(obj = {}) {
    if (Object.keys(obj).length === 0){
        return {}
    }

    else{
        let invertObject = {}
        for (let keys in obj){
        let setKey = obj[keys]
        let setValue = keys
        invertObject[setKey] = setValue

        }

        return invertObject
        
     }     
    }
    


module.exports = invert;