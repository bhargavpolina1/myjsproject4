function defaults(obj = {}, defaultProps = {}) {
    if (Object.keys(obj).length === 0|| Object.keys(defaultProps).length === 0) {
        return {}
    }

    else {
        let defaultsObject = {}
        for (let keys in obj) {
            defaultsObject[keys] = obj[keys]
        }
        for (let keys in defaultProps) {
            if (keys in defaultsObject) {
                continue
            }else{
                defaultsObject[keys] = defaultProps[keys]
            }
        }
        return defaultsObject
    }

}


module.exports = defaults;